import argparse
import os
import sys
from PIL import Image

def load_coordinates(filename):
    """ Load coordinates for each images """

    # Data will be stored in a dictionary
    file_coord = {}

    row_count = 0

    with open(filename, "r") as file_handler:
        lines = file_handler.readlines()

    for line in lines:
        if not line.strip():
            # Blank line means new row
            row_count += 1
            continue
        try:
            image_name, coords = tuple(line.split("|"))
            image_name = image_name.strip()
            x, y = coords.strip().split(",")
            x = int(x)
            y = int(y)
        except ValueError:
            print("Incorrect file format")
            print("line: {}".format(line))
            return False

        file_coord[image_name] = {'x': x, 'y': y}

    if row_count != 3:
        print("Incorrect number of rows: {}".format(count))
        return False

    return file_coord

def verify_images(images):
    """ Check if all files exist and dimensions are the same """

    not_exist = [f for img in images if not os.path.exists(img)]
    if not_exist:
        print("{} not found".format(not_exist))
        return False

    image_name = images[0]
    w, h = Image.open(image_name).size  
    images_count = len(images)

    for i in range(1,images_count):
        width, height = Image.open(images[i]).size
        if w != width or h != height:
            msg_error = ((
                "At least, size of {file1} "
                "is not equal to size of {file2}"
            ))
            print(msg_error.format(file1=image_name, file2=images[i]))
            return False

    return True


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=(
        "{} --folder <folder containing images>".format(__file__)
    ))

    parser.add_argument('--folder',
                        dest="folder",
                        help='Folder containing images')

    arguments = parser.parse_args()

    if not arguments.folder:
        print(parser.description)
        sys.exit()

    if not os.path.exists(arguments.folder):
        print("Folder {} does not exist".format(arguments.folder))
        sys.exit()

    # Load image names and coordinates from specified file
    filename = "coord.txt"
    if not os.path.exists(filename):
        print("File with coordinates does not exist")
        sys.exit()

    data = load_coordinates(filename)
    if not data:
        sys.exit()

    # Store image names only
    images = list(data.keys())
    # Add relative path to folder containing images
    # FOLDER/image.jpg,...
    images = ["{}/{}".format(arguments.folder, img) for img in images]
    print("Veryfying images..."),
    correct = verify_images(images)
    if not correct:
        system.exit()

    # Assuming that all images have the same size,
    # take the size of the first one
    img_width, img_height = Image.open(images[0]).size
    # Now calculate the needed dimensions for the big image
    # containing all of them    
    num_rows = 3
    num_columns = 4
    width, height = (num_rows * img_width, num_columns * img_height)
    # Now create the big image
    blank_image = Image.new('RGBA', (width, height), (255,255,255,0))

    # Put each single image in the the big image
    # using the right coordinates
    print("Generating composition..")
    for key, value in data.items():
        source_image = Image.open("{}/{}".format(arguments.folder, key))
        width, height = source_image.size        
        box = (value['x'], value['y'], value['x'] + width, value['y'] + height)        
        blank_image.paste(source_image, box)

    # Save the composition
    print("Saving composition...")
    blank_image.save("result.jpg")
